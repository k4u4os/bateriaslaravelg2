@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Turma 2D2 - Grupo 2</h1>
        <h2>Participantes:</h2>
        <hr>
        <table class="table table-striped table-bordered">
            <tr>
                <th>Matrícula</th>
                <th>Nome</th>
                <th>Função</th>
            </tr>
            <tr>
                <td>0072548</td>
                <td>Kauã Oliveira da Silva</td>
                <td>Gerente</td>
            </tr>
            <tr>
                <td>0072555</td>
                <td>Isabella Heloísa J. do Nascimento</td>
                <td>Blade, Model e Rotas</td>
            </tr>
            <tr>
                <td>0072542</td>
                <td>Ana Clara Alves Belmonte Galvão</td>
                <td>Banco de Dados, Migrations e Seeder</td>
            </tr>
            <tr>
                <td>0073607</td>
                <td>Bruno Andrade Alves</td>
                <td>Desenvolvimento CSS</td>
            </tr>
            <tr>
                <td>0072536</td>
                <td>Gabriel Riquelme Moura Magalhães</td>
                <td>HTML e Controller</td>
            </tr>
        </table>
        <img class="grupo" src="imgs/fotoprogaut.png" alt="componentes do grupo">
    </main>

@endsection 

@section('rodape')
    <h4>Rodapé da página principal<h4>
@endsection
