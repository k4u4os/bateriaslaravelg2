@extends('templates.base')

@section('conteudo')
<header>
  <h1><a href="/index.html" class="baterias">Baterias</a></h1>
</header>


<main>
  <p></p>

  <p>
    Primeiramente, utiliza-se um multímetro para efetuar a medição de sua tensão interna (sem carga), representado por "E".
    Para calcular a tensão externa (com carga), representado por Vr, é feita uma conexão com o resistor de referência com o 
    multímetro e a pilha/bateria. Com os resultados obtidos na etapa anterior, calcula-se, através da fórmula, a resistência interna "r".
  </p>
  <img src="/imgs/CodeCogsEqn (3).png">
  <p>
    Os valores foram colocados em uma tabela e um programa em javascript, que calcula automaticamente, com os valores em um objeto JSON,
    o valor da resistência interna e insere na tabela de <a href="/Paginas/medicoes.html">medições</a>.
  </p>
</main>


@endsection

@section('footlose')
<h4> Rodapé Procedimento</h4>
@endsection
