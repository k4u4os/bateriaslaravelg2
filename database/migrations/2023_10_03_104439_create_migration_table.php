<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('migration', function (Blueprint $table) {
            $table->id();
            $table->string('pilha_bateria');
            $table->decimal('tensao_nominal', 7, 3);
            $table->integer('capacidade_corrente');
            $table->decimal('tensao_sem_carga', 7, 3);
            $table->decimal('tensao_com_carga', 7, 3);
            $table->decimal('resistencia_carga', 4, 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('migration');
    }
};

