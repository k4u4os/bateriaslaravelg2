<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MedicoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('medicoes')->insert(
            [
                [
                    'pilha_bateria' => 'Pilha Alcalina Duracel AA',
                    'tensao_nominal' => 1.5,
                    'capacidade_corrente' => 2800,
                    'tensao_sem_carga' => 1.304,
                    'tensao_com_carga' => 1.286,
                    'resistencia_carga' => 23.7,
                ],
                [
                    'pilha_bateria' => 'Bateria 12V 7Ah',
                    'tensao_nominal' => 12,
                    'capacidade_corrente' => 7000,
                    'tensao_sem_carga' => 10.48,
                    'tensao_com_carga' => 10.32,
                    'resistencia_carga' => 23.7,
                ],
                [
                    'pilha_bateria' => 'Bateria recarregável Elgin 9v',
                    'tensao_nominal' => 9,
                    'capacidade_corrente' => 250,
                    'tensao_sem_carga' => 8.53,
                    'tensao_com_carga' => 6.23,
                    'resistencia_carga' => 23.7,
                ],
            ]
        );
    }
}

